# CI/CD Gig Demo

## What's this ?

This demo shows some CI/CD functionalities, it is really interactive let's try it and run a pipeline !

## Need CI/CD skills ?

Order a gig : [**laborX**](https://laborx.com/freelancers/users/id269365)

## Hello World

|  Variable  |     Value     | Example |
|:----------:|:-------------:|:-------:|
| `SAY_HELLO` |  *your name* |  john |

### Will:
    - simply run a job saying hello.

## Automating the creation of new features

|  Variable  |     Value     | Exemple |
|:----------:|:-------------:|:-------:|
| `BRANCH` |  *feature description* | change-banner-color |
| `JIRA` |  *JIRA ID* |  0001 |
| `TYPE` |  *feature type* | maintenance |

### Will:
    - Create a branch from the running one with a fixed pattern name
    - Create a draft Merge Request targeting the running one
    - Assign the MR to the user running the pipeline 

The point is to ensure that a JIRA task exists before starting to develop, make sure that all the branches has the same pattern name and create a MR at the very beginning of development so that other developers can easily follow the progress.

## Artifacts

|  Variable  |     Value     | Exemple |
|:----------:|:-------------:|:-------:|
| `BUILD` |  *app version* | 1.0.0 |
| `TESTS` |  *tests activation* |  true |

### Will:
    - Build the app (here it is an empty app having just a version field)
    - Place the built app in the artifacts
    - Then if tests are activated, tests jobs will get the app in the artifacts test it and put a report in the artifacts

## Email tests report

If tests are activated, a *manual* job will propose to send a email containing the tests reports.

## Multi project pipeline

|  Variable  |     Value     | Exemple |
|:----------:|:-------------:|:-------:|
| `SAY_HI` |  *your name* | John |

Will run a pipeline on another [**project**](https://gitlab.com/Antoine-Genonceau/triggered-project) saying hi. 
